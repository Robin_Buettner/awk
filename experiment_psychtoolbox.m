% Clear the workspace and the screen
sca;
close all;
clearvars;

%%%%%%%% SETTINGS %%%%%%%%
subject = 1; % one to four
n_pictures = 1   ;
screenNumber = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%

try 
    PsychDefaultSetup(2);
    Screen('Preference', 'SkipSyncTests', 1);  % Without this the Screen crashes because it can't ciompute the refresh interval

    % Define black and white
    white = WhiteIndex(screenNumber);
    black = BlackIndex(screenNumber);
    grey = white / 2;
    inc = white - grey;

    % Screen setup
    [window, windowRect] = PsychImaging('OpenWindow', screenNumber, grey); % Open an on screen window
    [screenXpixels, screenYpixels] = Screen('WindowSize', window); % Get the size of the on screen window
    ifi = Screen('GetFlipInterval', window); % Query the frame duration

    [xCenter, yCenter] = RectCenter(windowRect); % Get the centre coordinate of the window
    Screen('BlendFunction', window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'); % Set up alpha-blending for smooth (anti-aliased) lines

    %Welcome the subject
    intro = imread('graphics/introduction.jpg');
    imageTexture = Screen('MakeTexture', window, intro); % Make the image into a texture
    Screen('FillRect', window, [0 0 0]); % Black background
    Screen('DrawTexture', window, imageTexture, [], [], 0);
    Screen('Flip', window); % Flip to the screen
    WaitSecs(1); 
    KbWait;

    % Set brightness
    brightness = imread('graphics/brightness.jpg');
    imageTexture = Screen('MakeTexture', window, brightness); % Make the image into a texture
    Screen('FillRect', window, [0 0 0]); % Black background
    Screen('DrawTexture', window, imageTexture, [], [], 0);
    Screen('Flip', window); % Flip to the screen
    WaitSecs(1); 
    KbWait;

    % Adjust size if DIN-A5 sheet until satisfied
    scalingloop = true;
    while scalingloop
        % Calculate scaling factor
        ShowCursor('CrossHair');
        ch = imread('graphics/crosshair.jpg');
        imageTexture = Screen('MakeTexture', window, ch); % Make the image into a texture
        Screen('FillRect', window, [0 0 0]); % Black background
        Screen('DrawTexture', window, imageTexture, [], [], 0);
        Screen('Flip', window, [], 1); % Flip to the screen

        boundaries = zeros(4,1);
        [clicks, x, y, whichButton] = GetClicks([window], 0.01);
        Screen('DrawText', window, '+', x-7, y-6, white); %x-7, y-6 hard corded correction for offset
        Screen('Flip', window, [], 1);
        boundaries(1) = x;
        boundaries(2) = y;

        [clicks, x, y, whichButton] = GetClicks([window], 0.01);
        Screen('DrawText', window, '+', x-7, y-6, white);
        Screen('Flip', window, []);
        boundaries(3) = x;
        boundaries(4) = y;

        height = abs((boundaries(4) - boundaries(2)));
        width = abs((boundaries(3) - boundaries(1)));
        WaitSecs(0.2);

        dinRect = [0 0 width height];
        centeredRect = CenterRectOnPointd(dinRect, xCenter, yCenter);
        Screen('FillRect', window, [1 1 1], centeredRect);
        Screen('Flip', window, [], 1);
        DrawFormattedText(window, 'Stimmt die Größe? j (ja), oder n (nein) drücken.', 'center', 'center', [0 0 0]);
        Screen('Flip', window);
        WaitSecs(1);
        [secs, keyCode, deltaSecs] = KbWait();
        if keyCode(74) == 1
            scalingloop = false;
        end
    end

    %Testing part
    testing = imread('graphics/test.jpg');
    imageTexture = Screen('MakeTexture', window, testing); % Make the image into a texture
    Screen('FillRect', window, [0 0 0]); % Black background
    Screen('DrawTexture', window, imageTexture, [], [], 0);
    Screen('Flip', window); % Flip to the screen
    WaitSecs(0.5); 
    KbWait;

    for i=1:1
        name = sprintf('graphics/test%d.png',i);

        % Cropping the image
        image = imread(name);     
        imageSizeOriginal = size(image);
        scaling = height/imageSizeOriginal(1);
        img = imresize(image, [height, imageSizeOriginal(2)*scaling]);
        imageSize = size(img);

        ci = [imageSize(1)/2, imageSize(2)/2, imageSize(1)/2];     % center and radius of circle ([c_row, c_col, r])
        [xx,yy] = ndgrid((1:imageSize(1))-ci(1),(1:imageSize(2))-ci(2));
        mask = uint8((xx.^2 + yy.^2)<ci(3)^2);

        cropped = uint8(zeros(size(img)));
        for j=1:3
            cropped(:,:,j) = img(:,:,j).*mask;
        end

        % Display rotated and circular images on screen
        switch i
            case 1
                angle = 0;
            case 2
                angle = -45;
            case 3
                angle = -180;
        end

        imageTexture = Screen('MakeTexture', window, cropped); % Make the image into a texture
        Screen('FillRect', window, [0 0 0]); % Black background
        Screen('DrawTexture', window, imageTexture, [], [], angle);
        Screen('Flip', window, [], 1); % Flip to the screen

        for k=1:5
            [clicks, x, y, whichButton] = GetClicks([window], 0.01);
            Screen('DrawText', window, '+', x-7, y-6, [1 0 0]);
            Screen('Flip', window, [], 1);
            
            xOnImage = round(((x - xCenter) + imageSize(2)/2)/scaling);
            yOnImage = round(((y - yCenter) + imageSize(1)/2)/scaling);

        end
        WaitSecs(0.3);

        Screen('FillRect', window, [0 0 0]); % Black background
        Screen('Flip', window);
        WaitSecs(0.7);
    end

    testend = imread('graphics/testende.jpg');
    imageTexture = Screen('MakeTexture', window, testend); % Make the image into a texture
    Screen('FillRect', window, [0 0 0]); % Black background
    Screen('DrawTexture', window, imageTexture, [], [], 0);
    Screen('Flip', window); % Flip to the screen
    WaitSecs(0.5); 
    KbWait;


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Main loop for the experiment

    % Create vector with image numbers for shuffeling
    numbers = zeros(160,1);
    orientations = zeros(160,1);
    for i=1:160
        numbers(i) = i+99;
    end

    % Vector for the four orientations
    count = 1;
    for i=1:160
        orientations(i) = count;
        count = count + 1;
        if count == 5
            count = 1;
        end
    end

    rng(42); % Seed
    numbers = numbers(randperm(length(numbers)));
    orientations = orientations(randperm(length(orientations)));

    switch subject
        case 1
            numbers = numbers;
            orientations = orientations;
        case 2
            numbers = numbers;
            orientations = orientations +1;
        case 3
            numbers = flipud(numbers); % Reverse vector
            orientations = flipud(orientations + 2); % Circular shift
        case 4
            numbers = flipud(numbers);
            orientations = flipud(orientations + 3);   
    end   
    orientations(orientations > 4) = orientations(orientations > 4) - 4; 

    results = zeros(n_pictures, 10);
    for i=1:n_pictures
        % Get images pseudorandomly
        name = sprintf('images/%d.png',numbers(i));

        % Cropping the image
        image = imread(name);     
        imageSize = size(image);
        scaling = height/imageSize(1);
        img = imresize(image, [height, imageSize(2)*scaling]);
        imageSize = size(img);

        ci = [imageSize(1)/2, imageSize(2)/2, imageSize(1)/2];     % center and radius of circle ([c_row, c_col, r])
        [xx,yy] = ndgrid((1:imageSize(1))-ci(1),(1:imageSize(2))-ci(2));
        mask = uint8((xx.^2 + yy.^2)<ci(3)^2);

        cropped = uint8(zeros(size(img)));
        for j=1:3
            cropped(:,:,j) = img(:,:,j).*mask;
        end

        % Display rotated and circular images on screen
        switch orientations(i)
            case 1
                angle = 0;
            case 2
                angle = -45;
            case 3
                angle = -180;
            case 4
                angle = -225;
        end

        imageTexture = Screen('MakeTexture', window, cropped); % Make the image into a texture
        Screen('FillRect', window, [0 0 0]); % Black background
        Screen('DrawTexture', window, imageTexture, [], [], angle);
        Screen('Flip', window, [], 1); % Flip to the screen

        for k=1:5
            [clicks, x, y, whichButton] = GetClicks([window], 0.01);
            Screen('DrawText', window, '+', x-7, y-6, [1 0 0]);
            Screen('Flip', window, [], 1);
            results(i,k) = round(((x - xCenter) + imageSize(2)/2)/scaling); % Transform klicks back to original image position
            results(i,k+5) = round(((y - yCenter) + imageSize(1)/2)/scaling);
        end
        WaitSecs(0.3);

        Screen('FillRect', window, [0 0 0]); % Black background
        Screen('Flip', window);
        WaitSecs(0.7);

        if mod(i, 40) == 0
            pause = imread('graphics/break.jpg');
            imageTexture = Screen('MakeTexture', window, pause); % Make the image into a texture
            Screen('FillRect', window, [0 0 0]); % Black background
            Screen('DrawTexture', window, imageTexture, [], [], 0);
            Screen('Flip', window); % Fli
            
            p to the screen
            WaitSecs(0.5); 
            KbWait;
        end
    end

    % Save results
    save(sprintf('results%d.mat', subject),'numbers','orientations','results');

    % Ending
    ende = imread('graphics/end.jpg');
    imageTexture = Screen('MakeTexture', window, ende); % Make the image into a texture
    Screen('FillRect', window, [0 0 0]); % Black background
    Screen('DrawTexture', window, imageTexture, [], [], 0);
    Screen('Flip', window); % Flip to the screen
    WaitSecs(4); 

catch
    % This section is executed in case an error happens in the
    % experiment code implemented between try and catch...
    ShowCursor;
    sca;
    psychrethrow(psychlasterror);
end
sca; % Clear screen